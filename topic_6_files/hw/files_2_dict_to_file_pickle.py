import pickle


def save_dict_to_file_pickle(my_file_name,my_dict):

    """
    Функция save_dict_to_file_pickle.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """
    if type(my_dict) != dict or len(my_dict) == 0:
        return "Must be not empty dict"
    if type(my_file_name) != str or len(my_file_name) == 0:
        return "Name must be not empty and string"
    with open(my_file_name, "wb") as f:
        pickle.dump(my_dict, f)

if __name__ == '__main__':
    save_dict_to_file_pickle("new_pickle.pkl", {"zaza": "123"})
