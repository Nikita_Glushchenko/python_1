def read_str_from_file (my_file_name):

    """
    Функция read_str_from_file.

    Принимает 1 аргумент: строка (название файла или полный путь к файлу).

    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """

    if type(my_file_name) != str or len(my_file_name) == 0:
        return "Name must be not empty and string"

    with open(my_file_name, 'r') as f:
        # прочитать файл построчно
        for line in f:
            print(line.strip())


if __name__ == '__main__':
    read_str_from_file("my_hw_file.txt")

