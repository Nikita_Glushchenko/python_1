def save_dict_to_file_classic(file_name, my_dict):

    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """
    if type(file_name) != str or len(file_name) == 0:
        return "Name must be not empty and string"
    if type(my_dict) != dict or len(my_dict) == 0:
        return "Dict must be not empty and dictionary"

    with open(file_name, "a", newline='\r\n') as f:         # "a" добавит текст из словаря в файл, "w" перезапишет // КАК РАБОТАЕТ newline???
        f.write(str(my_dict))


if __name__ == '__main__':
    save_dict_to_file_classic("my_hw_file.txt", {'key_1': 'value_1',
                                                  "key_2": "value_2"})
