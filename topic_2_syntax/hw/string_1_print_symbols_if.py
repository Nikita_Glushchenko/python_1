"""
Функция print_symbols_if.

Принимает строку.

Если строка нулевой длины, то вывести строку "Empty string!".

Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'

Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
"""


def print_symbols_if(abc):
    s = str(abc)
    n = len(s)
    if n == 0:
        return "Empty string!"
    elif n > 5:
        return s[0:3] + s[n-3:n]
    else:
        return s[0:1]*n


if __name__ == "__main__":
    print(print_symbols_if(""))
    print(print_symbols_if("123asdadad321"))
    print(print_symbols_if("12345"))


