"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.
"""


def arithmetic(arg1, arg2, op):
    if op == "+":
        return arg1 + arg2
    elif op == "-":
        return arg1 - arg2
    elif op == "*":
        return arg1 * arg2
    elif op == "/":
        return arg1 / arg2
    else:
        return "Unknowm operator"


if __name__ == '__main__':
    print(arithmetic(3, 4, '+'))
    print(arithmetic(3, 4, '-'))
    print(arithmetic(3, 4, '*'))
    print(arithmetic(3, 4, '/'))
    print(arithmetic(3, 4, 'sss'))
