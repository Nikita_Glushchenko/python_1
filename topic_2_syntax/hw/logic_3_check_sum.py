"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""

def check_sum(char1, char2, char3):
    if char1+char2==char3 or char2+char3==char1 or char1+char3==char2:
        return "true"
    else:
        return "false"

if __name__ == '__main__':
    print(check_sum(5, 4, 3))
    print(check_sum(5, 2, 3))
