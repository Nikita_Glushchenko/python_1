"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(n):
    if n <= 0:
        return "Must be > 0!"
    elif n == int:
        return "Must be int!"
    else:
        even = odd = 0
        while n>0:
            if n % 2 == 0:
                even += 1
            else:
                odd += 1
            n = n // 10
        print(odd)


if __name__ == "__main__":
    print(count_odd_num(-154))
    print(count_odd_num(0))
    print(count_odd_num(13544))

