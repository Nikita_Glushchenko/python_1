"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    acc = 0
    for i in range(112, 0, -3):
        acc = acc + i
    print(acc)


if __name__ == "__main__":
    print(sum_1_112_3())


