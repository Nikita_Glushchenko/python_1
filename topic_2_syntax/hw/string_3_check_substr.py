"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substring(s1, s2):
    a = len(s1)
    b = len(s2)
    if s1 == s2:
        return "False"
    elif s1 == 0 or s2 == 0:
        return "True"
    elif a < b and s1 in s2:
        return "True"
    elif b < a and s2 in s1:
        return "True"
    else:
        return "False"


if __name__ == "__main__":
    print(check_substring("mama", "mama"))
    print(check_substring("mama", ""))
    print(check_substring("mama", "mama mia"))
    print(check_substring("mama mia", "mama"))
    print(check_substring("mama", "123"))

