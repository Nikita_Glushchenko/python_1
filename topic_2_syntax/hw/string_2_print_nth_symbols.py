"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


def print_nth_symbols(s1, n):
    x = int(len(s1))
    if n <= 0:
        return "Must be > 0!"
    elif n != int:
        return 'Must be int!'
    elif n > x:
        return ""
    else:



if __name__ == "_main__":
    print(print_nth_symbols("abc", "-3"))
