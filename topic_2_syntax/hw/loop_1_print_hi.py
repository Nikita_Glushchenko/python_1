"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
"""


def print_hi(n):
    print("Hi, friend!" * n)


if __name__ == "__main__":
    print(print_hi(3))
    print(print_hi(8))
