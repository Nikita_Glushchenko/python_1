"""
Функция if_3_do.

Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""

def if_3_do(char):
    if char > 3:
        return char+10
    else:
        return char-10

if __name__ == '__main__':
    print(if_3_do(5))
    print(if_3_do(2))


