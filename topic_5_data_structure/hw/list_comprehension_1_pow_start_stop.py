def pow_start_stop(start, stop):

    """
    Функция pow_start_stop.

    Принимает числа start, stop.

    Возвращает список состоящий из квадратов значений от start до stop (не включая).

    Пример: start=3, stop=6, результат [9, 16, 25].

    Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
    """

    if type(start) != int or type(stop) != int:
        return 'Start and Stop must be int!'
    else:
        return list([x ** 2 for x in range(start, stop, 1)])

if __name__ == '__main__':
    print(pow_start_stop(3, 6))
    print(pow_start_stop(3, 9))
    print(pow_start_stop(3, ""))
