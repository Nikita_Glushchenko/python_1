"""
Функция get_words_by_translation.

Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то ‘Can’t find English word: {word}’.

Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
"""


def get_words_by_translation(ru_eng: dict, eng: str):
    if type(ru_eng) != dict:
        return 'Dictionary must be dict!'
    if type(eng) != str:
        return 'Word must be str!'
    if len(ru_eng) == 0:
        return 'Dictionary is empty!'
    if len(eng) == 0:
        return 'Word is empty!'

    result = []

    for my_keys, my_values in ru_eng.items():
        if eng in my_values:
            result.append(my_keys)
    return result if len(result) > 0 else print(f"Can not find English word: {eng}")


if __name__ == '__main__':
    print(get_words_by_translation({"мама": "mother", "папа": "father", "сестра": "sister"}, "sister"))
    print(get_words_by_translation({"мама": "mother", "папа": "father", "сестра": "sister"}, ""))
    print(get_words_by_translation({}, ""))
    print(get_words_by_translation("ololo", ""))
    print(get_words_by_translation({"мама": "mother", "папа": "father", "сестра": "sister"}, 11))
