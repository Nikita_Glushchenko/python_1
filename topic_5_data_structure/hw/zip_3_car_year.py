import itertools


def zip_car_year(car_list, year_list):

    """
    Функция zip_car_year.

    Принимает 2 аргумента: список с машинами и список с годами производства.

    Возвращает список с парами значений из каждого аргумента, если один список больше другого,
    то заполнить недостающие элементы строкой “???”.

    Подсказка: zip_longest.

    Если вместо списков передано что-то другое, то возвращать строку 'Must be list!'.
    Если список (хотя бы один) пуст, то возвращать строку 'Empty list!'.
    """

    if type(car_list) != list or type(year_list) != list:
        return 'Must be list!'
    if len(car_list) == 0 or len(year_list) == 0:
        return 'Empty list!'

    result_list = list(itertools.zip_longest(car_list, year_list, fillvalue="???"))

    return result_list


if __name__ == '__main__':
    print(zip_car_year(["lada", "toyota", "nissan"], ["1988", "2000", "2010"]))
    print(zip_car_year(["lada", "toyota", "nissan"], ["1988", "2000", "2010", "2020"]))
    print(zip_car_year(["lada", "toyota", "nissan", "opel"], ["1988", "2000", "2010"]))
    print(zip_car_year([], ["1988", "2000", "2010"]))
    print(zip_car_year(["lada", "toyota", "nissan"], 123))
