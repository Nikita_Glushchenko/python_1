"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""

def zip_names(my_list, my_set):
    if type(my_list) != list:
        return 'First arg must be list!'
    if type(my_set) != set:
        return 'Second arg must be set!'
    if len(my_list) == 0:
        return 'Empty list!'
    if len(my_set) == 0:
        return 'Empty set!'

    return list(zip(my_list, my_set))

if __name__ == '__main__':
    print(zip_names(["nik", "kate", "john", "abraham"], {"volkov", "demidov", "vlasov"}))


