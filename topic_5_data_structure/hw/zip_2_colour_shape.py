"""
Функция zip_colour_shape.

Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.
Но если вместо tuple передана str, то конвертируем str -> tuple.

Если list пуст, то возвращать строку 'Empty list!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""


def zip_colour_shape(color_list, shape_tuple):
    if type(color_list) != list:
        return 'First arg must be list!'
    if type(shape_tuple) != tuple:
        return 'Second arg must be tuple!'
    if len(color_list) == 0:
        return 'Empty list!'
    if len(shape_tuple) == 0:
        return 'Empty tuple!'
    if type(shape_tuple) == str:
        return tuple(shape_tuple)
    return list(zip(color_list, shape_tuple))


if __name__ == '__main__':
    print(zip_colour_shape(["red", "green", "blue"], ("circle", "triangle", "square")))

