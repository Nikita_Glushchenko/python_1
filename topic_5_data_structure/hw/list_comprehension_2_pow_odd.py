"""
Функция pow_odd.

Принимает число n.

Возвращает список длиной n, состоящий из квадратов нечетных чисел в диапазоне от 0 до n (не включая).

Пример: n = 7, четные числа [1, 3, 5], результат [1, 9, 25]

Если n не являюется int, то вернуть строку 'Must be int!'.
"""


def pow_odd(n):
    if type(n) != int:
        return 'Must be int!'
    return list([x ** 2 for x in range(0, n, 1) if x % 2 != 0])


if __name__ == '__main__':
    print(pow_odd(7))
    print(pow_odd(12))
    print(pow_odd(""))
