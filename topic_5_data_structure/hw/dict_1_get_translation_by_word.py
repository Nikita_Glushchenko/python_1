"""
Функция get_translation_by_word.

Принимает 2 аргумента:
    ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
    слово для поиска в словаре (ru).

Возвращает все варианты переводов (list), если такое слово есть в словаре,
если нет, то ‘Can’t find Russian word: {word}’.

Если вместо словаря передано что-то другое, то возвращать строку 'Dictionary must be dict!'.
Если вместо строки для поиска передано что-то другое, то возвращать строку 'Word must be str!'.

Если словарь пустой, то возвращать строку 'Dictionary is empty!'.
Если строка для поиска пустая, то возвращать строку 'Word is empty!'.
"""


def get_translation_by_word(my_dict, my_word):

    if type(my_dict) != dict:
        return "Dictionary must be dict!"
    if type(my_word) != str:
        return 'Word must be str!'
    if len(my_dict) == 0:
        return 'Dictionary is empty!'
    if len(my_word) == 0:
        return 'Word is empty!'
    else:
        return my_dict.get(my_word, f"Can’t find Russian word: {my_word}")


if __name__ == '__main__':
    print(get_translation_by_word({"asd": 1, "asdsad": 2, "dsfdsf": 3, "wweew": 4}, "aaaaasd"))
    print(get_translation_by_word({"asd": 1, "asdsad": 2, "dsfdsf": 3, "wweew": 4}, 1))
    print(get_translation_by_word({"asd": 1, "asdsad": 2, "dsfdsf": 3, "wweew": 4}, "asd"))
    print(get_translation_by_word(["a", "b", "c"], "2"))
    print(get_translation_by_word({}, "asd"))
    print(get_translation_by_word({"asd": 1, "asdsad": 2, "dsfdsf": 3, "wweew": 4}, ""))




