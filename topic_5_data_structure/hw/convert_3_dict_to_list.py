"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает (список ключей,
            список значений,
            количество уникальных элементов в списке ключей,
            количество уникальных элементов в списке значений).

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
"""


def dict_to_list(my_dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    else:
        for key in my_dict.keys():
            print([key])
        for value in my_dict.values():
            print([value])


if __name__ == '__main__':
    dict_to_list({'a': 1, 'b': 2, "c": 3})