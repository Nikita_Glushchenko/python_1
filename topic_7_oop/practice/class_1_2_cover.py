from topic_7_oop.practice.class_1_1_page import Page


class Cover(Page):
    def __init__(self, n, c, color, title):
        super().__init__(n, c)
        self.color = color
        self.title = title

    def print_content(self):
        print(f'Станица {self.color} цвета\nЗаголовок: {self.title}')
        super(Cover, self).print_content()
        # эквивалентно super().print_content()


if __name__ == '__main__':
    c = Cover(0, 'bobobo', "Синий", "Лучшая книга века!")

    c.print_content()
