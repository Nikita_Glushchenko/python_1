"""
Класс EvenIntIterator.

Аргументы: целое число n.

Перегрузить методы __init__, __iter__ и __next__ таким образом чтобы
при итерировании по экземпляру EvenIntIterator метод next возвращает следующее четное число в диапазоне [0, n),
если произошел выход за пределы этого диапазона - ошибка StopIteration.
"""