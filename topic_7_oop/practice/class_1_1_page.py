class Page:
    """
    Класс Страница
    Поля: номер страницы, контент
    Методы:
        вывести контент,
        поиск строки в контенте (содержится ли строка в контенте?)
    """

    def __init__(self, n, c):
        self.num = n
        self.content = c

    def print_content(self):
        print(f'Страница {self.num}: {self.content}')

    def is_string_in_content(self, string_to_find):
        return string_to_find in self.content


if __name__ == '__main__':
    page1 = Page(1, 'lololol')
    page1.print_content()
    print(page1.is_string_in_content('lo'))
    print(page1.is_string_in_content('mo'))
