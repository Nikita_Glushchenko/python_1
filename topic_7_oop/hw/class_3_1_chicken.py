class Chicken:

    """
    Класс Chicken.

    Поля:
        имя: name,
        номер загона: corral_num,
        сколько яиц в день: eggs_per_day.

    Методы:
        (static) get_sound: вернуть строку 'Ко-ко-ко',
        get_info: вернуть строку вида:
            "Имя курицы: name
             Номер загона: corral_num
             Количество яиц в день: eggs_per_day"
        __lt__: вернуть результат сравнения количества яиц (<)
    """

    def __init__(self, name, corral_num, eggs_per_day):
        self.name = name
        self.corral_num = corral_num
        self.eggs_per_day = eggs_per_day

    def get_info(self):
        print(f"Имя курицы: {self.name}\n Номер загона: {self.corral_num}\n Количество яиц в день: {self.eggs_per_day}")

    def __lt__(self, other):
        return
