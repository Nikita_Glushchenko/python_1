class Human:

    """
    Класс Human.

    Поля:
        age,
        first_name,
        last_name.

    При создании экземпляра инициализировать поля класса.

    Создать метод get_age, который возвращает возраст человека.

    Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

    Перегрузить оператор __str__, который возвращает строку в виде "Имя: first_name last_name Возраст: age".
    """

    def __init__(self, age, first_name, last_name):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    def print_age(self):
        print(self.age)

    def __eq__(self, other):
        return self.age == other.age, self.first_name == other.first_name, self.last_name == other.last_name

    def __str__(self):
        return str(f"Имя: {self.first_name} {self.last_name} Возраст: {self.age}")


man1 = Human(13, "Vasiliy", "Nikolaev")
man2 = Human(13, "Vasiliy", "Nikolaev")
man3 = Human(15, "Dima", "Ivanov")

print(man1.__str__())
print(man1 == man2)
print(man1 == man3)







