class Farm:
    import random

    """
    Класс Farm.
    
    Поля:
        животные (list из произвольного количества Goat и Chicken): animals
                            (вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
        наименование фермы: name,
        имя владельца фермы: owner.
    
    Методы:
        get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
        get_chicken_count: вернуть количество куриц на ферме,
        get_animals_count: вернуть количество животных на ферме,
        get_milk_count: вернуть сколько молока можно получить в день,
        get_eggs_count: вернуть сколько яиц можно получить в день.
    """

    def __init__(self, animals, farm_name, farm_owner):
        self.animals = animals
        self.farm_name = farm_name
        self.farm_owner = farm_owner

    def