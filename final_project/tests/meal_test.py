import pytest
from final_project.meal import Meal


#bread = Meal("хлеб", "0,5 KG", 1, 1, 1, "35 RUR", "09.12.2020", "07.12.2020", "15.12.2020")
def test_init():
    name = "хлеб"
    weight = "1 KG"
    height = 1
    width = 1
    storage_place = 12345
    price = "55 RUR"
    date_of_receipt = "03.01.2021"
    production_date = "03.01.2021"
    best_before_date = "13.01.2021"

    bread = Meal(name, weight, height, width, storage_place, price, date_of_receipt, production_date, best_before_date)

    assert bread.name == name
    assert bread.weight == weight
    assert bread.height == height
    assert bread.width == width
    assert bread.storage_place == storage_place
    assert bread.price == price
    assert bread.date_of_receipt == date_of_receipt
    assert bread.production_date == production_date
    assert bread.best_before_date == best_before_date

