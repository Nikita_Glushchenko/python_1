import pytest
from final_project.materials import Materials


# window = Materials("Окно", "100 KG", 10, 10, 8, "15000 RUR", "09.12.2020", "Строительство дома")
def test_init():
    name = "Окно"
    weight = "100 KG"
    height = 10
    width = 10
    storage_place = 12
    price = "15 000 RUR"
    date_of_receipt = "10.12.2020"
    appointment = "Строительство дома"
    window = Materials(name, weight, height, width, storage_place, price, date_of_receipt, appointment)

    assert window.name == name
    assert window.weight == weight
    assert window.height == height
    assert window.width == width
    assert window.storage_place == storage_place
    assert window.price == price
    assert window.date_of_receipt == date_of_receipt
    assert window.appointment == appointment
