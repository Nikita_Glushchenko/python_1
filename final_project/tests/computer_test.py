import pytest
from final_project.computer import Computer


def test_init():
    name = "Macbook"
    weight = "2 KG"
    height = 1
    width = 1
    storage_place = 12
    price = "100 000 RUR"
    date_of_receipt = "10.12.2020"
    device_model = "AIR"
    computer_type = "Ноутбук"
    device_manufacturer = "Apple"
    pc = Computer(name, weight, height, width, storage_place, price, date_of_receipt,device_manufacturer, device_model,
                  computer_type)

    assert pc.name == name
    assert pc.weight == weight
    assert pc.height == height
    assert pc.width == width
    assert pc.storage_place == storage_place
    assert pc.price == price
    assert pc.date_of_receipt == date_of_receipt
    assert pc.device_model == device_model
    assert pc.computer_type == computer_type
    assert pc.device_manufacturer == device_manufacturer
