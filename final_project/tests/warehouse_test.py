import pytest

from final_project.product import Product
from final_project.electronics import Electronics
from final_project.computer import Computer
from final_project.materials import Materials
from final_project.meal import Meal
from final_project.mobile_phone import MobilePhone
from final_project.souvenir import Souvenir
from final_project.warehouse import Warehouse


def test_init():
    title = "My wh"

    wh = Warehouse(title)

    assert wh.products == dict()
    assert wh.title == title


def test_create_wh():
    title = ""

    wh = Warehouse.create_warehouse(title=title)


def test_add_product():
    title = "my wh"
    warehouse1 = Warehouse(title)

    bread = Meal("хлеб", "0,5 KG", 1, 1, 1, "35 RUR", "09.12.2020", "07.12.2020", "15.12.2020")
    milk = Meal("молоко", "1 KG", 1, 1, 2, "55 RUR", "09.12.2020", "09.12.2020", "12.12.2020")
    meat = Meal("мясо", "2 KG", 1, 1, 3, "700 RUR", "09.12.2020", "08.12.2020", "12.12.2020")
    magnet1 = Souvenir("магнит 1", "0,1 KG", 1, 1, 4, "135 RUR", "09.12.2020", "Not frg.")
    magnet2 = Souvenir("магнит 2", "0,1 KG", 1, 1, 5, "135 RUR", "09.12.2020", "Not frg.")
    mirror1 = Souvenir("Зеркало 1", "0,1 KG", 1, 1, 6, "235 RUR", "09.12.2020", "FRG.")
    mirror2 = Souvenir("Зеркало 2", "0,1 KG", 1, 1, 7, "235 RUR", "09.12.2020", "FRG.")
    window = Materials("Окно", "100 KG", 10, 10, 8, "15000 RUR", "09.12.2020", "Строительство дома")
    door = Materials("Дверь", "100 KG", 10, 10, 9, "5000 RUR", "09.12.2020", "Строительство дома")
    pc1 = Computer("Macbook", "2 KG", 1, 1, 10, "100 000 RUR", "10.12.2020", "Apple", "AIR", "Ноутбук")
    pc2 = Computer("Imac", "12 KG", 1, 1, 11, "150 000 RUR", "10.12.2020", "Apple", "Imac", "Моноблок")
    phone1 = MobilePhone("iPhone", "1 KG", 1, 1, 12, "50 000 RUR", "10.12.2020", "Apple", "5S", "iOS")
    phone2 = MobilePhone("Pixel", "1 KG", 1, 1, 13, "45 000 RUR", "10.12.2020", "Google", "5XL", "Android")

    warehouse1.add_product(bread)
    warehouse1.add_product(milk)
    warehouse1.add_product(meat)
    warehouse1.add_product(magnet1)
    warehouse1.add_product(magnet2)
    warehouse1.add_product(mirror1)
    warehouse1.add_product(mirror2)
    warehouse1.add_product(window)
    warehouse1.add_product(door)
    warehouse1.add_product(pc1)
    warehouse1.add_product(pc2)
    warehouse1.add_product(phone1)
    warehouse1.add_product(phone2)

    assert milk.product_type in warehouse1.products.keys()
    assert magnet1.product_type in warehouse1.products.keys()
    assert mirror1.product_type in warehouse1.products.keys()
    assert window.product_type in warehouse1.products.keys()
    assert pc1.product_type in warehouse1.products.keys()
    assert phone2.product_type in warehouse1.products.keys()
