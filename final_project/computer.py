from final_project.product import Product
from final_project.electronics import Electronics


class Computer(Electronics, Product):
    def __init__(self, name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer,
                 device_model, computer_type):
        super().__init__(name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer,
                         device_model)
        self.computer_type = computer_type

    product_type = "Компьютер"

    def __str__(self):
        return f"Computer ({super().__str__} | compute type: {self.computer_type})"

