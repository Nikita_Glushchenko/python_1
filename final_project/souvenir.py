from final_project.product import Product


class Souvenir(Product):
    def __init__(self, name, weight, height, width, storage_place, price, date_of_receipt, fragile_mark):
        super().__init__(name, weight, height, width, storage_place, price, date_of_receipt)
        self.fragile_mark = fragile_mark

    product_type = "Сувенир"

    def __str__(self):
        return f"Souvenir ({super().__str__()} | fragile: {self.fragile_mark})"

#проверка целости рандомом при поступлении