# import openpyxl
import pickle
import os

from final_project.product import Product
from final_project.electronics import Electronics
from final_project.computer import Computer
from final_project.materials import Materials
from final_project.meal import Meal
from final_project.mobile_phone import MobilePhone
from final_project.souvenir import Souvenir


# book = openpyxl.Workbook()
# sheet = book.active
# book.save("warehouse_book.xlsx")


class Warehouse:

    def __init__(self, title, should_load=False):
        self.products = {}

        self.title = str(title)

        self.backup_filename = f"wh_backup_{self.title}.pkl"

        if should_load:
            self.load_state()
        else:
            self.products = {}

    @classmethod
    def create_warehouse(cls, title, should_load_backup=False):
        if len(title) <= 0:
            print("Название склада не может быть пустым!")
        else:
            return cls(title, should_load_backup)

    def delete_warehouse(self):
        try:
            os.remove(self.backup_filename)
        except FileNotFoundError:
            pass

    def save_state(self):
        with open(self.backup_filename, "wb") as file:
            pickle.dump(self.products, file)

    def load_state(self):
        try:
            with open(self.backup_filename, "rb") as file:
                self.products = pickle.load(file)
        except FileNotFoundError as my_ex:
            self.products = {}
            print(my_ex)

    def add_product(self, product):
        product_type = product.product_type
        if self.products.get(product_type, None) is None:
            self.products[product_type] = []

        self.products[product_type].append(product)

        self.save_state()

    def add_product_by_category(self, category: str, products: list):
        """
        Принимает список продуктов и добавляет в соответствующую категорию,
        если категория не совпадает, то исключаем.
        :param category: тип продукта
        :param products: лист с продуктами
        :return:
        """
        for product in products:
            if product.product_type != category:
                raise Exception("Наименование продукта не совпадает с заявленным")
            self.add_product(product)

    def __str__(self):
        result_str = ""
        result_str += f"Warehouse name: {self.title}\n\n"
        for product_type, products in self.products.items():
            result_str += f"{product_type}:\n"
            for product in products:
                result_str += f"\t{product}\n"
        return result_str

    def get_product_wh_info_str(self):
        result_str = ""
        for prod_type, wh_products in self.products.items():
            result_str += f"{prod_type}:\n"
            for product in wh_products:
                result_str += f"\t{product}\n"
        return result_str

    def get_products_by_type(self, prod_type):
        products_by_type = self.products.get(prod_type, [])
        res_str = f"Category: {prod_type}\n"
        for product in products_by_type:
            res_str += f"\t{product}\n"
        return res_str

    def get_product_type_with_max_count(self):
        """
        получить список типов с наибольшей численностью
        :return: вернет список типов с наибольшей численностью
        """
        if len(self.products) == 0:
            return []

        p_t_counter = {}
        for product_type, products in self.products.items():
            p_t_counter[product_type] = len(products)

        max_counter = max(p_t_counter.values())
        res_list = []

        for product_type, counter in p_t_counter.items():
            if counter == max_counter:
                res_list.append(product_type)

        return f"Max count product: {res_list}"


if __name__ == '__main__':
    warehouse2 = Warehouse.create_warehouse("")
    warehouse1 = Warehouse("АШАН")
    bread = Meal("хлеб", "0,5 KG", 1, 1, 1, "35 RUR", "09.12.2020", "07.12.2020", "15.12.2020")
    milk = Meal("молоко", "1 KG", 1, 1, 2, "55 RUR", "09.12.2020", "09.12.2020", "12.12.2020")
    meat = Meal("мясо", "2 KG", 1, 1, 3, "700 RUR", "09.12.2020", "08.12.2020", "12.12.2020")
    magnet1 = Souvenir("магнит 1", "0,1 KG", 1, 1, 4, "135 RUR", "09.12.2020", "Not frg.")
    magnet2 = Souvenir("магнит 2", "0,1 KG", 1, 1, 5, "135 RUR", "09.12.2020", "Not frg.")
    mirror1 = Souvenir("Зеркало 1", "0,1 KG", 1, 1, 6, "235 RUR", "09.12.2020", "FRG.")
    mirror2 = Souvenir("Зеркало 2", "0,1 KG", 1, 1, 7, "235 RUR", "09.12.2020", "FRG.")
    window = Materials("Окно", "100 KG", 10, 10, 8, "15000 RUR", "09.12.2020", "Строительство дома")
    door = Materials("Дверь", "100 KG", 10, 10, 9, "5000 RUR", "09.12.2020", "Строительство дома")
    pc1 = Computer("Macbook", "2 KG", 1, 1, 10, "100 000 RUR", "10.12.2020", "Apple", "AIR", "Ноутбук")
    pc2 = Computer("Imac", "12 KG", 1, 1, 11, "150 000 RUR", "10.12.2020", "Apple", "Imac", "Моноблок")
    phone1 = MobilePhone("iPhone", "1 KG", 1, 1, 12, "50 000 RUR", "10.12.2020", "Apple", "5S", "iOS")
    phone2 = MobilePhone("Pixel", "1 KG", 1, 1, 13, "45 000 RUR", "10.12.2020", "Google", "5XL", "Android")

    warehouse1.add_product(bread)
    warehouse1.add_product(milk)
    warehouse1.add_product(meat)
    warehouse1.add_product(magnet1)
    warehouse1.add_product(magnet2)
    warehouse1.add_product(mirror1)
    warehouse1.add_product(mirror2)
    warehouse1.add_product(window)
    warehouse1.add_product(door)
    warehouse1.add_product(pc1)
    warehouse1.add_product(pc2)
    warehouse1.add_product(phone1)
    warehouse1.add_product(phone2)

    print(warehouse2)
    print(warehouse1)
    print("--------------------------------------------------")
    print(warehouse1.get_products_by_type("Сувенир"))
    print("--------------------------------------------------")
    print(warehouse1.get_product_type_with_max_count())

    prod_list = [Souvenir("магнит 1", "0,1 KG", 1, 1, 4, "135 RUR", "09.12.2020", "Not frg."),
                 Souvenir("магнит 2", "0,1 KG", 1, 1, 5, "135 RUR", "09.12.2020", "Not frg."),
                 Souvenir("Зеркало 1", "0,1 KG", 1, 1, 6, "235 RUR", "09.12.2020", "FRG."),
                 Souvenir("Зеркало 2", "0,1 KG", 1, 1, 7, "235 RUR", "09.12.2020", "FRG.")]

    warehouse1.add_product_by_category("Сувенир", prod_list)

    warehouse1.add_product_by_category("Электроника", prod_list)

    print(warehouse1)
