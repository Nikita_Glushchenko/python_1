from final_project.electronics import Electronics


class MobilePhone(Electronics):
    def __init__(self, name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer,
                 device_model, operation_system):
        super().__init__(name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer,
                         device_model)
        self.operation_system = operation_system

    product_type = "Мобильный телефон"

    def __str__(self):
        return f"Mobile phone ({super().__str__} | OS: {self.operation_system})"

