from final_project.product import Product


class Materials(Product):
    def __init__(self, name, weight, height, width, storage_place, price, date_of_receipt, appointment):
        super().__init__(name, weight, height, width, storage_place, price, date_of_receipt)
        self.appointment = appointment

    product_type = "Строй материал"

    def __str__(self):
        return f"Construction material ({super().__str__()} | appointment: {self.appointment})"
