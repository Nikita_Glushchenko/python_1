import telebot
from telebot import types
import pickle

from final_project.warehouse import Warehouse
from final_project.product import Product
from final_project.electronics import Electronics
from final_project.computer import Computer
from final_project.materials import Materials
from final_project.meal import Meal
from final_project.mobile_phone import MobilePhone
from final_project.souvenir import Souvenir

token = '1460547332:AAFbdCwyPFCs3HAJZbF1CFkOJhFVh3W4qVA'
bot = telebot.TeleBot(token)

# {user_id: [ wh1, wh2]}
wh_state = {}

# {user_id_1: message.from_user.id1, user_id_2: message.from_user.id2}
user_state = {}

# {user_id1: current_wh, user_id1: None}
user_current_wh = {}

backup_file_name = "bot_backup.pkl"

hideBoard = types.ReplyKeyboardRemove()


def save_state():
    with open(backup_file_name, "wb") as file:
        pickle.dump({"wh_state": wh_state,
                     "user_current_wh": user_current_wh,
                     "user_state": user_state},
                    file)


def load_state():
    global wh_state
    global user_state
    global user_current_wh

    try:
        with open(backup_file_name, "rb") as file:
            bot_state = pickle.load(file)
            wh_state = bot_state.get("wh_state", {})
            user_state = bot_state.get("user_state", {})
            user_current_wh = bot_state.get("user_current_wh", {})
    except FileNotFoundError as my_ex:
        print(f"Error during loading backup file: {my_ex}")
        wh_state = {}
        user_state = {}
        user_current_wh = {}


@bot.message_handler(commands=["warehouse_list"])
def wh_list(message):
    user_whs = wh_state.get(message.from_user.id, [])
    if len(user_whs) == 0:
        bot.send_message(message.chat.id, "У вас нет складов!\n"
                                          "Для добавления используйте команду /add_warehouse")
    else:
        markup = types.InlineKeyboardMarkup()

        for index, wh in enumerate(user_whs):
            markup.add(types.InlineKeyboardButton(text=str(wh.title), callback_data=index + 1))

        bot.send_message(message.chat.id,
                         "Можно выбрать склад для просмотра товаров",
                         reply_markup=markup)


@bot.callback_query_handler(func=lambda call: True)
def query_handler(call):
    global user_current_wh

    bot.answer_callback_query(callback_query_id=call.id, text='Информация о товарах')

    current_wh = wh_state[call.from_user.id][int(call.data) - 1]
    user_current_wh[call.from_user.id] = current_wh  # зафиксировать текущий выбранный склад
    save_state()

    product_info = current_wh.get_product_wh_info_str()

    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")
    bot.send_message(call.message.chat.id, f"{current_wh}\n\nХотите добавить продукт?" if
    len(product_info) > 0 else f"{current_wh}Нет ни одного продукта! Хотите добавить?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(call.message, add_product_yes_no_answer)


def add_product_yes_no_answer(message):
    if message.text == "Да":
        current_wh = user_current_wh.get(message.from_user.id)
        if current_wh is None:
            bot.send_message(message.from_user.id, "Склад не выбран. Проверьте командой /warehouse_list",
                             reply_markup=hideBoard)

        else:
            product_list_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                              one_time_keyboard=True,
                                                              row_width=5)
            product_list_keyboard.row("Еда", "Сувенир", "Стройматериал", "Компьютер", "Мобильный телефон")
            bot.send_message(message.chat.id,
                             "Какой продукт добавить?",
                             reply_markup=product_list_keyboard)
            bot.register_next_step_handler(message, add_choose_product_type)
    else:
        bot.send_message(message.from_user.id, "Ок", reply_markup=hideBoard)


def add_choose_product_type(message):
    if message.text == "Еда":
        bot.send_message(message.chat.id, "Введите значения полей через запятую с пробелом:"
                                          "\n(1)name, (2)weight, (3)height, (4)width, (5)storage_place, (6)price, "
                                          "(7)date_of_receipt, (8)production_date, (9)best_before_date",
                         reply_markup=hideBoard)
        bot.register_next_step_handler(message, add_meal)

    elif message.text == "Сувенир":
        bot.send_message(message.chat.id, "Введите значения полей через запятую с пробелом:"
                                          "\n(1)name, (2)weight, (3)height, (4)width, (5)storage_place, (6)price, "
                                          "(7)date_of_receipt, (8)fragile_mark",
                         reply_markup=hideBoard)
        bot.register_next_step_handler(message, add_souvenir)

    elif message.text == "Стройматериал":
        bot.send_message(message.chat.id, "Введите значения полей через запятую с пробелом:"
                                          "\n(1)name, (2)weight, (3)height, (4)width, (5)storage_place, (6)price, "
                                          "(7)date_of_receipt, (8)appointment",
                         reply_markup=hideBoard)
        bot.register_next_step_handler(message, add_material)

    elif message.text == "Компьютер":
        bot.send_message(message.chat.id, "Введите значения полей через запятую с пробелом:"
                                          "\n(1)name, (2)weight, (3)height, (4)width, (5)storage_place, (6)price, "
                                          "(7)date_of_receipt, (8)device_manufacturer, (9)device_model, "
                                          "(10)computer_type",
                         reply_markup=hideBoard)
        bot.register_next_step_handler(message, add_computer)

    elif message.text == "Мобильный телефон":
        bot.send_message(message.chat.id, "Введите значения полей через запятую с пробелом:"
                                          "\n(1)name, (2)weight, (3)height, (4)width, (5)storage_place, (6)price, "
                                          "(7)date_of_receipt, (8)device_manufacturer, (9)device_model, "
                                          "(10)operation_system",
                         reply_markup=hideBoard)
        bot.register_next_step_handler(message, add_mobile_phone)


def add_meal(message):
    try:
        name, weight, height, width, storage_place, price, date_of_receipt, production_date, \
        best_before_date = message.text.split(", ")
    except ValueError:
        bot.send_message(message.from_user.id, "Выберите склад и ведите корректные данные о продукте!")
    else:

        new_meal = Meal(name, weight, height, width, storage_place, price,
                        date_of_receipt, production_date, best_before_date)

        current_wh = user_current_wh.get(message.from_user.id)
        if current_wh is None:
            bot.send_message(message.from_user.id, "Склад не выбран. Проверьте командой /warehouse_list")

        else:
            current_wh.add_product(new_meal)
            bot.send_message(message.from_user.id,
                             f"Продукт добавлен: {new_meal}")
            save_state()


def add_souvenir(message):
    try:
        name, weight, height, width, storage_place, price, date_of_receipt, fragile_mark = message.text.split(", ")
    except ValueError:
        bot.send_message(message.from_user.id, "Выберите склад и ведите корректные данные о продукте!")
    else:

        new_souvenir = Souvenir(name, weight, height, width, storage_place, price,
                                date_of_receipt, fragile_mark)

        current_wh = user_current_wh.get(message.from_user.id)
        if current_wh is None:
            bot.send_message(message.from_user.id, "Склад не выбран. Проверьте командой /warehouse_list")

        else:
            current_wh.add_product(new_souvenir)
            bot.send_message(message.from_user.id,
                             f"Продукт добавлен: {new_souvenir}")
            save_state()


def add_material(message):
    try:
        name, weight, height, width, storage_place, price, date_of_receipt, appointment = message.text.split(", ")
    except ValueError:
        bot.send_message(message.from_user.id, "Выберите склад и ведите корректные данные о продукте!")
    else:

        new_material = Materials(name, weight, height, width, storage_place, price,
                                 date_of_receipt, appointment)

        current_wh = user_current_wh.get(message.from_user.id)
        if current_wh is None:
            bot.send_message(message.from_user.id, "Склад не выбран. Проверьте командой /warehouse_list")

        else:
            current_wh.add_product(new_material)
            bot.send_message(message.from_user.id,
                             f"Продукт добавлен: {new_material}")
            save_state()


def add_mobile_phone(message):
    try:
        name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer, device_model, \
        operation_system = message.text.split(", ")
    except ValueError:
        bot.send_message(message.from_user.id, "Выберите склад и ведите корректные данные о продукте!")
    else:

        new_phone = MobilePhone(name, weight, height, width, storage_place, price,
                                date_of_receipt, device_manufacturer, device_model, operation_system)

        current_wh = user_current_wh.get(message.from_user.id)
        if current_wh is None:
            bot.send_message(message.from_user.id, "Склад не выбран. Проверьте командой /warehouse_list")

        else:
            current_wh.add_product(new_phone)
            bot.send_message(message.from_user.id,
                             f"Продукт добавлен: {new_phone}")
            save_state()


def add_computer(message):
    try:
        name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer, device_model, \
        computer_type = message.text.split(", ")
    except ValueError:
        bot.send_message(message.from_user.id, "Выберите склад и ведите корректные данные о продукте!")
    else:

        new_computer = Computer(name, weight, height, width, storage_place, price,
                                date_of_receipt, device_manufacturer, device_model, computer_type)

        current_wh = user_current_wh.get(message.from_user.id)
        if current_wh is None:
            bot.send_message(message.from_user.id, "Склад не выбран. Проверьте командой /warehouse_list")

        else:
            current_wh.add_product(new_computer)
            bot.send_message(message.from_user.id,
                             f"Продукт добавлен: {new_computer}")
            save_state()


@bot.message_handler(commands=["add_warehouse"])
def add_wh(message):
    new_user = message.from_user.id
    if user_state.get(message.from_user.id) is None:
        user_state[message.from_user.id] = []
        user_state[message.from_user.id].append(new_user)
        bot.send_message(message.chat.id, "Напишите название первого склада:")
        bot.register_next_step_handler(message, add_new_user_wh)

    else:
        bot.send_message(message.chat.id, "Напишите название нового склада:")
        bot.register_next_step_handler(message, add_new_user_wh)


def add_new_user_wh(message):
    new_wh = Warehouse.create_warehouse(message.text, user_state.get(message.from_user.id))

    if wh_state.get(message.from_user.id) is None:
        wh_state[message.from_user.id] = []
    wh_state[message.from_user.id].append(new_wh)
    ##wh_state[message.from_user.id].remove(new_wh)

    bot.send_message(message.chat.id, f"Склад добавлен!\n {new_wh}")

    save_state()


@bot.message_handler(commands=["start"])
def help_handler(message):
    bot.send_message(message.chat.id, f"Привет {message.from_user.first_name}!" f"\nПомощь доступна по команде /help ")


@bot.message_handler(commands=["help"])
# HELP не работает если bot.send_message(message.chat.id, answer, parse_mode='html')!!!!
# ERROR - TeleBot: "A request to the Telegram API was unsuccessful.
# Error code: 400. Description:
# Bad Request: can't parse entities: Unsupported start tag "" at byte offset 27"
def help_handler(message):
    answer = f"Вот, что я умею:" \
             f"\n/warehouse_list для получения списка складов" \
             f"\n/add_warehouse для добавления склада" \
             f"\n/remove_warehouse для удаления склада"
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=["remove_warehouse"])
#def help_handler(message):
#    bot.send_message(message.chat.id, f"Для удаления склада обратитесь к администратору!")
def wh_list(message):
    user_whs = wh_state.get(message.from_user.id, [])
    if len(user_whs) == 0:
        bot.send_message(message.chat.id, "У вас нет складов!\n"
                                          "Для добавления используйте команду /add_warehouse")
    else:
        bot.send_message(message.chat.id, "Введите названия склада, который хотите удалить:")
        bot.register_next_step_handler(message, remove_warehouse)


def remove_warehouse(message):
    wh_rem_title = message.text
    rem_wh = Warehouse(message.text, user_state.get(message.from_user.id))
    user_whs = wh_state.get(message.from_user.id, dict())
    wh_titles_list = []
    for index, wh in enumerate(user_whs):
        wh_titles_list.append(str(wh.title))

    if wh_rem_title in wh_titles_list:
        for i, o in enumerate(user_whs):
            if o.title == wh_rem_title:
                del user_whs[i]
                break

        rem_wh.delete_warehouse()
        bot.send_message(message.chat.id, f'Склад {wh_rem_title} удален!\n ')
        save_state()
    else:
        bot.send_message(message.chat.id, "Склада с таким именем не найдено!")


if __name__ == '__main__':
    print('Loading state...')
    load_state()
    print('Starting bot...')
    bot.polling(none_stop=True, interval=0)
