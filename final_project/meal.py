from final_project.product import Product


class Meal(Product):
    def __init__(self, name, weight, height, width, storage_place, price, date_of_receipt, production_date,
                 best_before_date):
        super().__init__(name, weight, height, width, storage_place, price, date_of_receipt)
        self.production_date = production_date
        self.best_before_date = best_before_date

    product_type = "Продукт питания"

    def __str__(self):
        return f"Meal ({super().__str__()} | production date: {self.production_date} | best before: {self.best_before_date})"

# проверка срока годности при поступлении
# вывод остатка срока годности
# предупреждение приближения окончания срока годности
