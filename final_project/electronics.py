from final_project.product import Product


class Electronics(Product):
    def __init__(self, name, weight, height, width, storage_place, price, date_of_receipt, device_manufacturer,
                 device_model):
        super().__init__(name, weight, height, width, storage_place, price, date_of_receipt)
        self.device_manufacturer = device_manufacturer
        self.device_model = device_model

    product_type = "Электроника"

    @property
    def __str__(self):
        return f"Electronics ({super().__str__()} | manufacturer: {self.device_manufacturer} | model: {self.device_model})"
